const fs = require('fs')
const url = require('url')
const minimatch = require('minimatch')
const sleep = require('then-sleep')

module.exports = async req => {
    const requestUrl = url.parse(req.url)

    const urlFilePath = `${requestUrl.pathname
        .substring(1)
        .replace(/\//g, '.')}${requestUrl.search ? requestUrl.search : ''}.json`

    // console.log(123, fs.readdirSync('./mock').find(x => true))
    const matchedFilePath = fs
        .readdirSync('./mock')
        .find(localFilePath => minimatch(urlFilePath, localFilePath))

    await sleep(3000)

    if (matchedFilePath) {
        return JSON.parse(fs.readFileSync(`./mock/${matchedFilePath}`))
    }

    throw new Error(
        `Could not find file '${
            urlFilePath
        }' or correspondent glob in directory.`
    )
}
