import React from 'react'
import Section from 'components/Section'
import { observer, inject } from 'mobx-react'
import InputFormField from 'components/InputFormField'
import SelectFormField from 'components/SelectFormField'

const RetailerSubSection = ({
    claim,
    countryOptions,
    retailerOptions,
    optionsAreLoading
}) => (
    <Section.Content>
        <SelectFormField
            id="country"
            label="Country"
            optionLabel={country => country.countryName}
            hideIfOneOption
            field={claim.country}
            options={countryOptions}
            optionsAreLoading={optionsAreLoading}
            dropdown
        />
        <SelectFormField
            id="retailer"
            label="Retailer"
            optionLabel={retailer =>
                `${retailer.dealerCode} - ${retailer.name}`
            }
            field={claim.retailer}
            options={retailerOptions}
            optionsAreLoading={optionsAreLoading}
        />
        <InputFormField
            id="retailerReference"
            label="Retailer reference"
            field={claim.retailerReference}
        />
        <InputFormField
            id="jobCardNumber"
            label="Job card number"
            field={claim.jobCardNumber}
        />
    </Section.Content>
)

export default inject(({ store }) => {
    const { claimStore: { claim } } = store
    return {
        optionsAreLoading:
            store.retailerStore.loading && store.countryStore.loading,
        countryOptions: store.retailerStore.countryOptions,
        retailerOptions: store.retailerStore.options(
            claim.country && claim.country.value
        ),
        claim
    }
})(observer(RetailerSubSection))
