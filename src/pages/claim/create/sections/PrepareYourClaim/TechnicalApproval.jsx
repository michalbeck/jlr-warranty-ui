import React from 'react'
import Section from 'components/Section'
import ButtonSelectFormField from 'components/ButtonSelectFormField'
import InputFormField from 'components/InputFormField'
import { observer, inject } from 'mobx-react'

const TechnicalApproval = ({ claim }) =>
    claim.claimType.value &&
    claim.campaignOption.value &&
    claim.campaignOption.value.id === 'X' && (
        <Section.Content
            subtitle="Technical approval"
            tooltip="Tooltip from Technical approval TBD"
        >
            <ButtonSelectFormField
                id="technicalRefType"
                label="Technical ref. type"
                field={claim.technicalRefType}
                options={[
                    { id: '1', label: 'No Technical Reference' },
                    { id: '2', label: 'FRED' },
                    { id: '3', label: 'Technical Assistance' },
                    { id: '4', label: 'EPQR' }
                ]}
                optionLabel={({ label }) => label}
            />
            {claim.technicalRefType.value &&
                claim.technicalRefType.value.id === '2' && (
                    <InputFormField
                        id="technicalRefNumber"
                        label="Technical ref. number"
                        field={claim.technicalRefNumber}
                    />
                )}
        </Section.Content>
    )

export default inject(({ store }) => {
    const { claimStore: { claim } } = store
    return {
        claim
    }
})(observer(TechnicalApproval))
