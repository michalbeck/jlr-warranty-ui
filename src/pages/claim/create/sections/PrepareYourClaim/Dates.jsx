import React from 'react'
import Section from 'components/Section'
import { observer, inject } from 'mobx-react'
import DateFormField from 'components/DateFormField'
import { Col, Row } from 'reactstrap'
import styled from 'styled-components'

const CenteredCol = styled(Col)`
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
`

const StyledDiv = styled.div`
    padding: 15px 45px;
    border-radius: 4px;
    box-shadow: 0 1px 1px 1px rgba(0, 0, 0, 0.08);
    text-transform: uppercase;
`

const StyledH2 = styled.h2`
    font-weight: bold;
`

const DatesFieldSubsection = ({ claim }) => (
    <Section.Content subtitle="Dates" tooltip="Tooltip from Dates TBD">
        <Row>
            <Col>
                <DateFormField
                    id="vehicleDeliveredDate"
                    label="Vehicle delivered"
                    field={claim.vehicleDeliveredDate}
                    maxDate={new Date().getTime()} // Today is max date
                />
                {claim.vehicle.value && (
                    <div>
                        <DateFormField
                            id="repairStartedDate"
                            label="Repair started"
                            field={claim.repairStartedDate}
                            minDate={claim.vehicleDeliveredDate.value}
                            maxDate={new Date().getTime()} // Today is max date
                        />
                        <DateFormField
                            id="repairStoppedDate"
                            label="Repair stopped"
                            field={claim.repairStoppedDate}
                            minDate={claim.repairStartedDate.value}
                            maxDate={new Date().getTime()} // Today is max date
                        />
                        <DateFormField
                            id="vehicleCollectedDate"
                            label="Vehicle collected"
                            field={claim.vehicleCollectedDate}
                            minDate={claim.repairStoppedDate.value}
                            maxDate={new Date().getTime()} // Today is max date
                        />
                    </div>
                )}
            </Col>
            {claim.daysInShop && (
                <CenteredCol sm={4}>
                    <StyledDiv>
                        <StyledH2>{claim.daysInShop}</StyledH2> day{claim.daysInShop !==
                            1 && 's'}{' '}
                        in shop
                    </StyledDiv>
                </CenteredCol>
            )}
        </Row>
    </Section.Content>
)

export default inject(({ store }) => {
    const { claimStore: { claim } } = store
    return {
        claim
    }
})(observer(DatesFieldSubsection))
