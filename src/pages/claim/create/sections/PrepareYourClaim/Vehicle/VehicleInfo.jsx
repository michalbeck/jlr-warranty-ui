import React from 'react'
import { observer } from 'mobx-react'
import { formatDate } from 'utils/dates'
import { FormGroup, Col } from 'reactstrap'
import JaguarSillouete from 'assets/jaguar-sillouette.svg'
import styled from 'styled-components'
import theme from 'styles/theme'

const StyledRow = styled(FormGroup)`
    border-top: 2px solid ${theme.jaguarBlueGrey3};
    border-bottom: 2px solid ${theme.jaguarBlueGrey3};
`

const VehicleInfo = ({
    distanceLastClaim,
    distanceUnit,
    dateLastClaim,
    description,
    countryName
}) => (
    <StyledRow row className="py-4 no-gutters">
        <Col>
            <img
                src={JaguarSillouete}
                alt="Jaguar Sillouette"
                title="Jaguar Sillouette"
                className="float-left mr-4 mt-1"
            />
            <h5 className="text-uppercase">
                {description || 'No description for Vehicle'}
            </h5>
            <div className="float-right">
                Country Sold:
                <span className="font-weight-bold"> {countryName || '-'}</span>
            </div>
            <div>
                <span className="mr-4">
                    Distance on last claim:{' '}
                    {!distanceLastClaim && typeof distanceLastClaim !== 'number'
                        ? '-'
                        : distanceLastClaim}
                    <span className="text-lowercase"> {distanceUnit}</span>
                </span>
                <span>Date on last claim: {formatDate(dateLastClaim)}</span>
            </div>
        </Col>
    </StyledRow>
)

export default observer(VehicleInfo)
