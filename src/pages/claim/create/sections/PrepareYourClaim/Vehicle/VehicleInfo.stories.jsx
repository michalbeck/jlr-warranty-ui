import React from 'react'

import { storiesOf } from '@storybook/react'
import { Form } from 'reactstrap'
import Section from 'components/Section'
import VehicleInfo from './VehicleInfo'

storiesOf('VehicleInfo', module)
    .addDecorator(story => (
        <div className="container">
            <Form>
                <Section order="1" title="Prepare your claim">
                    <Section.Content subtitle="Vehicle">
                        {story()}
                    </Section.Content>
                </Section>
            </Form>
        </div>
    ))
    .add('with all info', () => (
        <VehicleInfo
            description="jaguar XE prestige 2.0L 4 cyl"
            dateLastClaim="2012-07-16T00:00:00.000Z"
            distanceUnit="KM"
            countryName="USA"
            distanceLastClaim={22345}
        />
    ))
    .add('with no info', () => <VehicleInfo />)
