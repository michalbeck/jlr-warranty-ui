import React, { Component } from 'react'
import FormField from 'components/FormField'
import Select from 'react-select'
import { Input, Label, Row, Col } from 'reactstrap'
import { observer } from 'mobx-react'
import { searchVehicles } from 'services'
import Vehicle from 'stores/Vehicle'
import Highlighter from 'react-highlight-words'

class Search extends Component {
    constructor(props) {
        super(props)
        const { registration, vin } = props.vehicle.value || {
            registration: '',
            vin: ''
        }

        this.state = {
            searchType: 'vin',
            registration: {
                value: registration,
                loading: false,
                status: registration ? 'validated' : 'new',
                errors: []
            },
            vin: {
                value: vin,
                loading: false,
                status: vin ? 'validate' : 'new',
                errors: []
            }
        }
    }

    handleSelectSearchType = ({ target: { value } }) => {
        this.setState(() => ({
            searchType: value
        }))
    }

    handleRegistrationInputChange = ({ target: { value } }) => {
        // Update value and set status to 'new' to invalidated validation
        this.setState(({ registration }) => ({
            registration: { ...registration, value, status: 'new', errors: [] }
        }))
    }

    handleRegistrationInputBlur = async () => {
        if (!this.state.registration.value) {
            // Empty registration, clear fields
            // Not found vehicle with VIN, add error
            this.props.vehicle.set(null)
            this.setState(({ registration, vin }) => ({
                registration: {
                    ...registration,
                    status: 'new',
                    errors: []
                },
                vin: { ...vin, status: 'new', value: '' }
            }))
            return
        }

        // Set as pending
        this.setState(({ registration }) => ({
            registration: { ...registration, status: 'pending' }
        }))

        // Search for vehicle registration
        const vehicles = await searchVehicles(
            this.props.retailer.value && this.props.retailer.value.id,
            'registration',
            this.state.registration.value
        )

        if (vehicles && vehicles[0]) {
            // Found vehicle, set state and vehicle property
            this.props.vehicle.set(Vehicle.create(vehicles[0]))
            this.setState(({ registration, vin }) => ({
                registration: {
                    ...registration,
                    status: 'validated',
                    value: vehicles[0].registration
                },
                vin: {
                    ...vin,
                    status: 'validated',
                    value: vehicles[0].id,
                    options: [
                        {
                            value: vehicles[0].id,
                            label: vehicles[0].id
                                ? vehicles[0].id
                                : `No VIN for ${vehicles[0].registration}`
                        }
                    ]
                }
            }))
        } else {
            // Not found vehicle with VIN, add error
            this.props.vehicle.set(null)
            this.setState(({ registration, vin }) => ({
                registration: {
                    ...registration,
                    status: 'error',
                    errors: [
                        'Could not find a registered vehicle with registration'
                    ]
                },
                vin: { ...vin, status: 'new', value: '' }
            }))
        }
    }

    handleVINSelect = selectedVehicle => {
        if (selectedVehicle) {
            // Selected valid vehicle, set state and vehicle property
            this.props.vehicle.set(Vehicle.create(selectedVehicle))
            this.setState(({ registration, vin }) => ({
                registration: {
                    ...registration,
                    status: 'validated',
                    errors: [],
                    value: selectedVehicle.registration
                        ? selectedVehicle.registration
                        : `No registration for ${selectedVehicle.id}`
                },
                vin: { ...vin, status: 'validated', value: selectedVehicle.id }
            }))
        } else {
            // Vehicle was cleared, return to previous situation
            this.props.vehicle.set(null)
            this.setState(({ registration, vin }) => ({
                registration: {
                    ...registration,
                    status: 'new',
                    errors: [],
                    value: ''
                },
                vin: { ...vin, status: 'new', value: '', options: null }
            }))
        }
    }

    handleSelectInputChange = async input => {
        if (input && input.length > 5) {
            this.setState(({ vin }) => ({
                vin: {
                    ...vin,
                    status: 'pending',
                    options: []
                }
            }))
            const vehicles = await searchVehicles(
                this.props.retailer.value && this.props.retailer.value.id,
                'vin',
                `*${input}*`
            )
            this.setState(({ vin }) => ({
                vin: {
                    ...vin,
                    options: vehicles.map(vehicle => ({
                        value: vehicle.vin,
                        label: vehicle.vin,
                        ...vehicle
                    })),
                    input,
                    status: 'new'
                }
            }))
        } else {
            this.setState(({ vin }) => ({
                vin: {
                    ...vin,
                    // when vehicle select, input is null so we need to maintain previous options
                    options: input ? [] : vin.options,
                    input
                }
            }))
        }
    }

    renderOption = option => (
        <Highlighter
            searchWords={[this.state.vin.input]}
            textToHighlight={option.label}
        />
    )

    renderDropDownHelperText = () => {
        const { vin } = this.state
        if (vin.input && vin.input.length > 5) {
            return vin.status === 'pending'
                ? 'Loading results, type to start a new search'
                : 'No results found'
        }

        return 'Type at least 6 chars to start search'
    }

    render() {
        const { searchType, registration, vin } = this.state
        const isVINTypeSelected = searchType === 'vin'

        return [
            <FormField
                key="vin"
                id="searchVehicle"
                label="Search vehicle by"
                status={vin.status}
                errors={vin.errors}
            >
                <Row>
                    <Col sm="2">
                        <Label check>
                            <Input
                                type="radio"
                                name="searchType"
                                value="vin"
                                checked={isVINTypeSelected}
                                onChange={this.handleSelectSearchType}
                            />{' '}
                            VIN
                        </Label>
                    </Col>
                    <Col sm="10">
                        <Select
                            onBlurResetsInput={false}
                            onCloseResetsInput={false}
                            onSelectResetsInput={false}
                            id="searchVehicle"
                            name="vin"
                            options={vin.options}
                            isLoading={vin.status === 'pending'}
                            optionRenderer={this.renderOption}
                            noResultsText={this.renderDropDownHelperText()}
                            onInputChange={input => {
                                this.handleSelectInputChange(input)
                                return input
                            }}
                            onChange={this.handleVINSelect}
                            value={vin.value}
                            placeholder="min 6 characters (8 recommended)"
                            clearable
                            filterOptions={options => options}
                            disabled={!isVINTypeSelected}
                        />
                    </Col>
                </Row>
            </FormField>,
            <FormField
                key="registration"
                id="searchVehicle"
                status={registration.status}
                errors={registration.errors}
            >
                <Row>
                    <Col sm="2">
                        <Label check>
                            <Input
                                type="radio"
                                name="searchType"
                                value="registration"
                                checked={!isVINTypeSelected}
                                onChange={this.handleSelectSearchType}
                            />{' '}
                            Registration
                        </Label>
                    </Col>
                    <Col sm="10">
                        <Input
                            maxLength="8"
                            type="text"
                            placeholder="max 8 characters"
                            disabled={isVINTypeSelected}
                            value={registration.value}
                            onChange={this.handleRegistrationInputChange}
                            onBlur={this.handleRegistrationInputBlur}
                        />
                    </Col>
                </Row>
            </FormField>
        ]
    }
}

export default observer(Search)
