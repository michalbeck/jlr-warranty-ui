import React from 'react'
import Section from 'components/Section'
import { observer, inject } from 'mobx-react'
import { Col, Row } from 'reactstrap'
import Spinner from 'react-spinkit'
import styled from 'styled-components'
import Search from './Search'
import VehicleInfo from './VehicleInfo'
import Details from './Details'

const CenteredCol = styled(Col)`
    text-align: center;
    margin: 20px 0;
`

const StyledSpinner = styled(Spinner)`
    display: inline-block;
    margin-right: 20px;
    top: 6px;
`

const VehicleFieldSubSection = ({
    vehicle,
    retailer,
    vehicleIsLoading,
    vehicleDistance,
    vehicleDistanceUnit,
    vehicleDistanceProgression,
    vehicleFullHistoryConfirmed
}) => (
    <Section.Content subtitle="Vehicle" tooltip="Tooltip from Vehicle TBD">
        <Search vehicle={vehicle} retailer={retailer} />
        {vehicleIsLoading ? (
            <Row>
                <CenteredCol>
                    <StyledSpinner
                        title="Please wait, pending loading information..."
                        name="folding-cube"
                        color="orange"
                        fadeIn="none"
                    />Please wait, vehicle details are being loaded...
                </CenteredCol>
            </Row>
        ) : (
            <div>
                {vehicle.value && <VehicleInfo {...vehicle.value} />}
                {vehicle.value && (
                    <Details
                        vehicle={vehicle}
                        vehicleDistance={vehicleDistance}
                        vehicleDistanceUnit={vehicleDistanceUnit}
                        vehicleDistanceProgression={vehicleDistanceProgression}
                        vehicleFullHistoryConfirmed={
                            vehicleFullHistoryConfirmed
                        }
                    />
                )}
            </div>
        )}
    </Section.Content>
)

export default inject(({ store }) => {
    const vehicleField = store.claimStore.claim.vehicle
    return {
        vehicle: vehicleField,
        retailer: store.claimStore.claim.retailer,
        vehicleDistance: store.claimStore.claim.vehicleDistance,
        vehicleDistanceUnit: store.claimStore.claim.vehicleDistanceUnit,
        vehicleDistanceProgression:
            store.claimStore.claim.vehicleDistanceProgression,
        vehicleFullHistoryConfirmed:
            store.claimStore.claim.vehicleFullHistoryConfirmed,
        vehicleIsLoading: vehicleField.value && vehicleField.value.isLoading
    }
})(observer(VehicleFieldSubSection))
