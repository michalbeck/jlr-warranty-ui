import React from 'react'
import FormField from 'components/FormField'
import CheckboxFormField from 'components/CheckboxFormField'
import Select from 'react-select'
import { Input, Row, Col } from 'reactstrap'
import { observer } from 'mobx-react'
import DistanceUnits from 'stores/ClaimStore/Field/DistanceUnit'

const Details = ({
    vehicleDistance,
    vehicleDistanceUnit,
    vehicleDistanceProgression,
    vehicleFullHistoryConfirmed
}) => (
    <div>
        <FormField
            id="distance"
            label="Distance"
            mandatory={vehicleDistance.mandatory}
            errors={vehicleDistance.errors}
            status={vehicleDistance.status}
        >
            <Row>
                <Col sm="3">
                    <Input
                        type="number"
                        id="distance"
                        name="distance"
                        value={vehicleDistance.value}
                        onBlur={() => vehicleDistance.validate()}
                        onChange={({ target: { value } }) =>
                            vehicleDistance.set(value)
                        }
                    />
                </Col>
                <Col sm="3">
                    <Select
                        id="distanceDimensions"
                        name="distanceDimensions"
                        clearable={false}
                        searchable={false}
                        onChange={({ value }) => {
                            vehicleDistanceUnit.set(value)
                            vehicleDistanceUnit.validate()
                        }}
                        options={DistanceUnits}
                        value={vehicleDistanceUnit.value}
                    />
                </Col>
            </Row>
        </FormField>
        <CheckboxFormField
            label="Other vehicle details"
            id="distanceProgression"
            valueLabel="Distance progression"
            field={vehicleDistanceProgression}
        />
        <CheckboxFormField
            id="fullServiceHistory"
            valueLabel="Full service history confirmed"
            field={vehicleFullHistoryConfirmed}
        />
    </div>
)

export default observer(Details)
