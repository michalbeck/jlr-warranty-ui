import React from 'react'
import Section from 'components/Section'
import ButtonSelectFormField from 'components/ButtonSelectFormField'
import SelectFormField from 'components/SelectFormField'
import { observer, inject } from 'mobx-react'

const ClaimType = ({ claim }) =>
    claim.vehicleDeliveredDate.value && (
        <Section.Content
            subtitle="Claim Type"
            tooltip="Tooltip from Claim Type TBD"
        >
            <ButtonSelectFormField
                id="claimType"
                label="Claim Type"
                field={claim.claimType}
                options={[
                    { id: '1', label: 'Vehicle Warranty' },
                    { id: '2', label: 'Goodwill' },
                    { id: '3', label: 'Parts Warranty' },
                    { id: '4', label: 'Campaign' },
                    { id: '5', label: 'Service Maintenance Plan' },
                    { id: '6', label: 'OTC' }
                ]}
                optionLabel={({ label }) => label}
                optionDisabled={({ id }) => id === '5' || id === '6'}
            />
            {claim.claimType.value && (
                <SelectFormField
                    id="campaignCode"
                    label="Campaign Code"
                    optionLabel={({ id, label }) => `${id} - ${label}`}
                    field={claim.campaignCode}
                    dropdown
                    options={[
                        {
                            id: '00365A',
                            label: 'Lorem Ipsum'
                        },
                        {
                            id: '00365B',
                            label: 'Lorem Ipsum'
                        },
                        {
                            id: '00365C',
                            label: 'Lorem Ipsum'
                        },
                        {
                            id: '00365D',
                            label: 'Lorem Ipsum'
                        }
                    ]}
                />
            )}
            {claim.claimType.value && (
                <SelectFormField
                    id="campaignOption"
                    label="Campaign Option"
                    optionLabel={({ id, label }) => `${id} - ${label}`}
                    field={claim.campaignOption}
                    dropdown
                    disabled={!claim.campaignCode.value}
                    options={[
                        {
                            id: 'B',
                            label: 'Configure ECM'
                        },
                        {
                            id: 'C',
                            label: 'Configure ECM DIDO'
                        },
                        {
                            id: 'X',
                            label: 'Lorem Ipsum'
                        }
                    ]}
                />
            )}
        </Section.Content>
    )

export default inject(({ store }) => {
    const { claimStore: { claim } } = store
    return {
        claim
    }
})(observer(ClaimType))
