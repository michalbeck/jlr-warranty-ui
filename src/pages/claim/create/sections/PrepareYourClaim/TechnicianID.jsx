import React from 'react'
import Section from 'components/Section'
import InputFormField from 'components/InputFormField'
import { observer, inject } from 'mobx-react'

const TechnicianID = ({ claim }) =>
    claim.claimType.value &&
    (!claim.campaignOption.value || claim.campaignOption.value.id !== 'X') && (
        <Section.Content
            subtitle="Technician ID"
            tooltip="Tooltip from Technician ID TBD"
        >
            <InputFormField
                id="technicianID"
                label="Technician ID"
                field={claim.technicianID}
            />
        </Section.Content>
    )

export default inject(({ store }) => {
    const { claimStore: { claim } } = store
    return {
        claim
    }
})(observer(TechnicianID))
