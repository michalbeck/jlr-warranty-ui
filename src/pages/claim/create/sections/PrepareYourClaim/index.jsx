import React from 'react'
import Section from 'components/Section'
import Retailer from './Retailer'
import Vehicle from './Vehicle'
import Dates from './Dates'
import ClaimType from './ClaimType'
import TechnicianID from './TechnicianID'
import TechnicalApproval from './TechnicalApproval'

const PrepareYourClaim = () => (
    <Section order="1" title="Prepare your claim">
        <Retailer />
        <Vehicle />
        <Dates />
        <ClaimType />
        <TechnicianID />
        <TechnicalApproval />
    </Section>
)

export default PrepareYourClaim
