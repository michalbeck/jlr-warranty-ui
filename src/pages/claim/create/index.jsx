import React, { Component } from 'react'
import { Form } from 'reactstrap'
import Section from 'components/Section'
import { inject, observer } from 'mobx-react'
import PrepareYourClaim from './sections/PrepareYourClaim'
import ActionBar from './ActionBar'

class CreateClaim extends Component {
    componentWillMount() {
        this.props.newClaim()
    }
    render() {
        return (
            <Form>
                <PrepareYourClaim />
                <ActionBar />
                <Section order="2" title="SROs and Parts" />
                <Section order="3" title="Prepare your claim" />
                <Section order="4" title="Problem description and narrative" />
                <Section order="5" title="Claim value" />
            </Form>
        )
    }
}

export default inject(({ store }) => ({
    newClaim: store.claimStore.newClaim
}))(observer(CreateClaim))
