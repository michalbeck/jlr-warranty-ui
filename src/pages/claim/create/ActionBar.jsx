import React, { Component } from 'react'
import { Col, Row, Button } from 'reactstrap'
import styled from 'styled-components'
import { observer, inject } from 'mobx-react'

const StyledButton = styled(Button)`
    letter-spacing: 0.05rem;
`

class ActionBar extends Component {
    render() {
        const { claim } = this.props
        return (
            claim.claimType.value && (
                <Row className="mb-3">
                    <Col className="clearfix">
                        <StyledButton
                            className="ml-3 float-right text-uppercase"
                            outline
                            color="primary"
                        >
                            submit for payment now
                        </StyledButton>
                        <StyledButton
                            className="float-right text-uppercase"
                            color="primary"
                        >
                            {claim.campaignOption.value &&
                            claim.campaignOption.value.id === 'X'
                                ? 'Continue'
                                : 'Save & Continue'}
                        </StyledButton>
                    </Col>
                </Row>
            )
        )
    }
}

export default inject(({ store }) => {
    const { claimStore: { claim } } = store
    return {
        claim
    }
})(observer(ActionBar))
