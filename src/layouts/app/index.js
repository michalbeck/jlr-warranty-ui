import React from 'react'
import { Route, Link } from 'react-router-dom'
import styled from 'styled-components'
import CreateClaimComponent from 'pages/claim/create'

const AppHome = () => (
    <div>
        <main>App Home Body placeholder</main>
    </div>
)

const ClaimHome = () => (
    <div>
        <header>
            <h2>Claim Home</h2>
        </header>
        <nav>
            <ul>
                <li>
                    <Link to="/claim/create">Create a claim</Link>
                </li>
                <li>
                    <Link to="/claim/edit">Edit a claim</Link>
                </li>
            </ul>
        </nav>
        <main>Claim Home Body placeholder</main>
    </div>
)

const Title = styled.h2`
    font-size: 24px;
    font-weight: bold;
`

const Subtitle = styled.h3`
    display: inline-block;
    font-size: 15px;
    padding: 10px 0;
`

const ClaimCreate = () => (
    <div>
        <header>
            <Title>Create Claim</Title>
            <Subtitle>* Mandatory field</Subtitle>
        </header>
        <main>
            <CreateClaimComponent />
        </main>
    </div>
)

const ClaimEdit = () => (
    <div>
        <header>
            <h2>Edit claim</h2>
        </header>
        <main>Edit Claim Body placeholder</main>
    </div>
)

const ClaimView = () => (
    <div>
        <header>
            <h2>Edit claim</h2>
        </header>
        <main>Edit Claim Body placeholder</main>
    </div>
)

const AppLayout = () => (
    <div className="container">
        <header>
            <p className="float-right">
                v{process.env.REACT_APP_VERSION || '?'}
            </p>
            <h2>JLR WASP 3.0</h2>
        </header>
        <nav>
            <ul>
                <li>
                    <Link to="/">App home</Link>
                </li>
                <li>
                    <Link to="/claim">Claim home</Link>
                </li>
            </ul>
        </nav>
        <main>
            <Route exact path="/" component={AppHome} />
            <Route exact path="/claim" component={ClaimHome} />
            <Route path="/claim/create" component={ClaimCreate} />
            <Route path="/claim/edit" component={ClaimEdit} />
            <Route path="/claim/view" component={ClaimView} />
        </main>
        <footer>Footer placeholder</footer>
    </div>
)

export default AppLayout
