import React from 'react'
import { IntlProvider } from 'react-intl'
import { HashRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { observer, inject } from 'mobx-react'

import colours from 'styles/colours'
import AppLayout from './app'

const App = inject('store')(
    observer(() => (
        <ThemeProvider theme={colours}>
            <IntlProvider locale="en">
                <HashRouter>
                    <AppLayout />
                </HashRouter>
            </IntlProvider>
        </ThemeProvider>
    ))
)

export default App
