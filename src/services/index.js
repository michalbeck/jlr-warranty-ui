import serviceUrl from 'config'
import format from 'string-format-obj'

const request = async endpoint => {
    const response = await fetch(serviceUrl(endpoint), {
        credentials: 'include'
    })
    if (response.status >= 400) {
        throw new Error('Response error', response)
    }

    return response.json()
}

export default request

export async function searchVehicles(dealershipContext, searchType, value) {
    const params = {
        dealershipContext,
        vin: '',
        registration: ''
    }

    params[searchType] = value

    const { records } = await request(
        format(
            'vehicle/search?dealershipContext={dealershipContext}&vin={vin}&registration={registration}',
            params
        )
    )
    return records
}
