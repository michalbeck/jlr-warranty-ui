import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import { getSnapshot } from 'mobx-state-tree'
import createRootStore from 'stores'
import './styles/index.css'
import registerServiceWorker from './registerServiceWorker'
import App from './layouts'

let store = createRootStore()

const renderApp = () => {
    ReactDOM.render(
        <Provider store={store}>
            <App />
        </Provider>,
        document.getElementById('root')
    )
}

// Initial rendering
renderApp()

registerServiceWorker()

if (module.hot) {
    module.hot.accept(['./stores'], () => {
        // Store definition changed, recreate a new one from old state
        const createRootStoreNext = require('stores').default // eslint-disable-line
        store = createRootStoreNext(getSnapshot(store))
        renderApp()
    })

    module.hot.accept(['./layouts'], () => {
        // Component definition changed, re-render app
        renderApp()
    })
}
