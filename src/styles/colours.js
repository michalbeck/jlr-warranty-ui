const colours = {
    jaguarRed: '#9e1b32',
    jaguarBlueGrey1: '#5b788f',
    jaguarBlueGrey2: '#95adac',
    jaguarBlueGrey3: '#cdd7d9',
    jaguarBlack: '#1e1e1e',
    jaguarFlatGrey1: '#525252',
    jaguarFlatGrey2: '#8c8c8c',
    jaguarFlatGrey3: '#c4c4c4',
    jaguarFlatGrey4: '#e8e8e8',
    jaguarFlatGrey5: '#f8f8f8',
    jaguarGreen: '#4caf50',
    jaguarRed1: '#d0021b',
    jaguarOrange: '#ef6c00'
}

export default colours
