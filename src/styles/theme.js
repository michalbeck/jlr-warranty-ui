import createTheme from 'styled-components-theme'
import colours from 'styles/colours'

const theme = createTheme(...Object.keys(colours))
export default theme
