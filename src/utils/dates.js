// @flow
import moment from 'moment'

export const formatDate = (timestamp?: number): string =>
    timestamp ? moment(timestamp).format('Do MMM YYYY') : '-'

export default formatDate
