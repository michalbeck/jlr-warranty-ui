export default function serviceUrl(service) {
    const proxyPrefix = process.env.REACT_APP_PROXY
        ? process.env.REACT_APP_PROXY
        : '/proxy/jlr/zpd'

    return `${
        process.env.NODE_ENV === 'production' ? '' : proxyPrefix
    }/dwm-api/dwmapi/${service}`
}
