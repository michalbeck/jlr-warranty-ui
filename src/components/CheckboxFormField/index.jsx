import React, { Component } from 'react'
import { Input, Label } from 'reactstrap'
import FormField from 'components/FormField'
import { observer } from 'mobx-react'
import styled from 'styled-components'

const CheckboxLabel = styled(Label)`
    user-select: none;
`

class CheckboxFormField extends Component {
    handleChange = ({ target: { checked } }) => {
        this.props.field.set(checked)
    }

    handleBlur = () => {
        this.props.field.validate()
    }

    render() {
        const { hidden, field, label, id, valueLabel } = this.props
        const { value, mandatory, errors } = field

        return (
            <FormField
                id={id}
                mandatory={mandatory}
                hidden={hidden}
                errors={errors}
                label={label}
            >
                <CheckboxLabel check>
                    <Input
                        id={id}
                        name={id}
                        className={`checkbox-input-${id}`}
                        type="checkbox"
                        checked={value}
                        onBlur={this.handleBlur}
                        onChange={this.handleChange}
                    />{' '}
                    {valueLabel}
                </CheckboxLabel>
            </FormField>
        )
    }
}

export default observer(CheckboxFormField)
