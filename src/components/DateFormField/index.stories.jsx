import React from 'react'

import { storiesOf } from '@storybook/react'
import { withKnobs } from '@storybook/addon-knobs'
// import { action } from '@storybook/addon-actions'
// import { linkTo } from '@storybook/addon-links'
import moment from 'moment'
import { Form } from 'reactstrap'
import BaseFieldSimulator from 'stores/ClaimStore/Field/BaseFieldSimulator'
import DateFormField from './index'

storiesOf('DateFormField', module)
    .addDecorator(withKnobs)
    .addDecorator(story => (
        <div style={{ margin: '20px' }}>
            <Form>{story()}</Form>
        </div>
    ))
    .add('disabled', () => (
        <BaseFieldSimulator value={1515319662065}>
            {field => (
                <DateFormField
                    id="simpleId"
                    label="simple label"
                    disabled
                    field={field}
                />
            )}
        </BaseFieldSimulator>
    ))
    .add('timestamp value displayed', () => (
        <BaseFieldSimulator value={1515319662065}>
            {field => (
                <DateFormField
                    id="simpleId"
                    label="simple label"
                    field={field}
                />
            )}
        </BaseFieldSimulator>
    ))
    .add('null value displayed', () => (
        <BaseFieldSimulator>
            {field => (
                <DateFormField
                    id="simpleId"
                    label="simple label"
                    field={field}
                />
            )}
        </BaseFieldSimulator>
    ))
    .add('disabled before minDate', () => (
        <BaseFieldSimulator>
            {field => (
                <DateFormField
                    id="simpleId"
                    label="simple label"
                    field={field}
                    minDate={moment()
                        .subtract(3, 'days')
                        .valueOf()}
                />
            )}
        </BaseFieldSimulator>
    ))
    .add('disabled after maxDate', () => (
        <BaseFieldSimulator>
            {field => (
                <DateFormField
                    id="simpleId"
                    label="simple label"
                    field={field}
                    maxDate={moment()
                        .add(3, 'days')
                        .valueOf()}
                />
            )}
        </BaseFieldSimulator>
    ))
    .add('disabled after and before dates', () => (
        <BaseFieldSimulator>
            {field => (
                <DateFormField
                    id="simpleId"
                    label="simple label"
                    field={field}
                    minDate={moment()
                        .subtract(3, 'days')
                        .valueOf()}
                    maxDate={moment()
                        .add(3, 'days')
                        .valueOf()}
                />
            )}
        </BaseFieldSimulator>
    ))
    .add('long label', () => (
        <BaseFieldSimulator>
            {field => (
                <DateFormField
                    id="simpleId"
                    label="very long form label to input"
                    field={field}
                />
            )}
        </BaseFieldSimulator>
    ))
