import React, { Component } from 'react'
import FormField from 'components/FormField'
import { observer } from 'mobx-react'
import 'react-dates/initialize'
import { SingleDatePicker } from 'react-dates'
import moment from 'moment'
import 'react-dates/lib/css/_datepicker.css'
import 'styles/react-dates-overrides.css'

// Docs https://github.com/airbnb/react-dates#singledatepicker

class DateFormField extends Component {
    state = {
        focused: false
    }

    handleChange = date => {
        this.props.field.set(date ? date.valueOf() : null)
    }

    handleFocusChange = ({ focused }) => {
        if (!focused) {
            this.props.field.validate()
        }
        this.setState({ focused })
    }

    render() {
        const {
            hidden,
            field,
            label,
            id,
            minDate,
            maxDate,
            disabled
        } = this.props
        const { mandatory, status, errors, value } = field

        return (
            <FormField
                id={id}
                label={label}
                mandatory={mandatory}
                hidden={hidden}
                errors={errors}
                status={status}
            >
                <SingleDatePicker
                    small
                    block
                    verticalSpacing={15}
                    placeholder={`Enter Date ${label}`}
                    date={value && moment(value)}
                    disabled={disabled}
                    onDateChange={this.handleChange} // PropTypes.func.isRequired
                    focused={this.state.focused} // PropTypes.bool
                    onFocusChange={this.handleFocusChange} // PropTypes.func.isRequired
                    reopenPickerOnClearDate
                    displayFormat="DD/MM/YYYY"
                    showDefaultInputIcon
                    showClearDate
                    initialVisibleMonth={() => moment().subtract(1, 'months')}
                    isOutsideRange={date =>
                        !date.isBetween(
                            minDate ? moment(minDate) : null,
                            maxDate ? moment(maxDate) : null,
                            'day',
                            '[]'
                        )
                    }
                    isDayHighlighted={date => date.isSame(new Date(), 'day')}
                />
            </FormField>
        )
    }
}

export default observer(DateFormField)
