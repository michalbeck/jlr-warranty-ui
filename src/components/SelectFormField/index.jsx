import React, { Component } from 'react'
import Select from 'react-select'
import { observer } from 'mobx-react'
import { reaction } from 'mobx'
import FormField from 'components/FormField'

class SelectFormField extends Component {
    componentWillMount() {
        this.reactionHandlers = []

        // Setup reactions
        this.reactionHandlers.push(
            reaction(
                () => this.props.options,
                options => {
                    // If there is only one option, set the value as the only option and validate
                    if (options.length === 1) {
                        this.props.field.set(this.props.options[0])
                        this.props.field.validate()
                    }
                }
            )
        )

        // If value is not included in options anymore, clear value
        this.reactionHandlers.push(
            reaction(
                () => [this.props.options, this.props.field.value],
                ([options, value]) => {
                    // Find by id because value can be a mobx object and options not
                    // FUTURE: If all mobx references, then can use options.include method
                    if (
                        !options.find(option => value && option.id === value.id)
                    ) {
                        this.props.field.set()
                    }
                }
            )
        )
    }

    componentDidUpdate(prevProps) {
        if (prevProps.value !== this.props.value) {
            this.handleBlur()
        }
    }

    componentWillUnmount() {
        // destroy reactions
        this.reactionHandlers.forEach(handler => handler())
    }

    handleChange = selectedOption => {
        this.props.field.set(selectedOption && selectedOption.entry)
    }

    handleBlur = () => {
        this.props.field.validate()
    }

    render() {
        const {
            options,
            label,
            id,
            field,
            optionLabel,
            hideIfOneOption,
            dropdown,
            optionsAreLoading,
            disabled
        } = this.props
        const { value, mandatory, status, errors } = field

        // Convert options provided into options for select
        const selectOptions = options.map(entry => ({
            value: entry.id,
            label: optionLabel(entry),
            entry
        }))

        return (
            <FormField
                id={id}
                label={label}
                mandatory={mandatory}
                hidden={hideIfOneOption && options.length === 1}
                errors={errors}
                status={status}
            >
                <Select
                    id={id}
                    name={id}
                    className={`select-input-${id}`}
                    options={selectOptions}
                    value={value && value.id}
                    placeholder={
                        optionsAreLoading
                            ? 'Please wait, loading options...'
                            : `Select a ${label.toLowerCase()} (${
                                  options.length
                              })`
                    }
                    clearable={options.length !== 1}
                    searchable={!dropdown}
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    isLoading={optionsAreLoading}
                    disabled={optionsAreLoading || disabled}
                />
            </FormField>
        )
    }
}

export default observer(SelectFormField)
