import React from 'react'
import { Col, FormGroup, Label, FormFeedback } from 'reactstrap'
import { observer } from 'mobx-react'
import styled from 'styled-components'
import Spinner from 'react-spinkit'
import exclamationError from 'assets/28-error.svg'
import tickValid from 'assets/28-tick.svg'

const ErrorFormFeedback = styled(FormFeedback)`
    display: block;
`

const FormField = ({
    id,
    label,
    children,
    mandatory,
    status,
    errors = [],
    hidden,
    className
}) =>
    hidden ? null : (
        <FormGroup className={className} row>
            <Label className="label" for={id} sm={3}>
                {label}
                {mandatory && '*'}
            </Label>
            <Col className="children" sm={7}>
                {children}
                {errors.map(error => (
                    <ErrorFormFeedback key={error}>{error}</ErrorFormFeedback>
                ))}
            </Col>
            <Col className="validations" sm={2}>
                {status === 'pending' && (
                    <Spinner
                        title="Please wait, pending loading information..."
                        name="folding-cube"
                        color="orange"
                    />
                )}
                {status === 'validated' && <img src={tickValid} alt="Valid" />}
                {status === 'error' && (
                    <img src={exclamationError} alt="Error" />
                )}
            </Col>
        </FormGroup>
    )

export default observer(FormField)
