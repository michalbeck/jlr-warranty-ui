import React from 'react'

import { storiesOf } from '@storybook/react'
import { withKnobs } from '@storybook/addon-knobs'
// import { action } from '@storybook/addon-actions'
// import { linkTo } from '@storybook/addon-links'
import { Form, Input } from 'reactstrap'
import FormField from './index'

storiesOf('FormField', module)
    .addDecorator(withKnobs)
    .addDecorator(story => (
        <div style={{ margin: '20px' }}>
            <Form>{story()}</Form>
        </div>
    ))
    .add('hidden', () => (
        <FormField id="simpleId" label="simple label" hidden>
            <Input
                type="text"
                name="simple"
                id="simpleId"
                placeholder="with a placeholder"
            />
        </FormField>
    ))
    .add('with simple input', () => (
        <FormField id="simpleId" label="simple label">
            <Input
                type="text"
                name="simple"
                id="simpleId"
                placeholder="with a placeholder"
            />
        </FormField>
    ))
    .add('is mandatory', () => (
        <FormField id="mandatoryId" label="mandatory label" mandatory>
            <Input
                type="email"
                name="mandatory"
                id="mandatoryId"
                placeholder="with a placeholder"
            />
        </FormField>
    ))
    .add('pending', () => (
        <FormField
            id="mandatoryId"
            label="simple label"
            mandatory
            status="pending"
        >
            <Input
                type="email"
                name="mandatory"
                id="mandatoryId"
                placeholder="with a placeholder"
            />
        </FormField>
    ))
    .add('validated without error', () => (
        <FormField
            id="mandatoryId"
            label="simple label"
            mandatory
            status="validated"
        >
            <Input
                type="email"
                name="mandatory"
                id="mandatoryId"
                placeholder="with a placeholder"
            />
        </FormField>
    ))
    .add('validated with error', () => (
        <FormField
            id="mandatoryId"
            label="simple label"
            status="error"
            errors={['Ops, something bad happened !']}
        >
            <Input
                type="email"
                name="mandatory"
                id="mandatoryId"
                placeholder="with a placeholder"
            />
        </FormField>
    ))
