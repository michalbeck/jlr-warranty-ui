import React, { Component } from 'react'
import { Input } from 'reactstrap'
import FormField from 'components/FormField'
import { observer } from 'mobx-react'

class InputFormField extends Component {
    handleChange = ({ target: { value } }) => {
        this.props.field.set(value)
    }

    handleBlur = () => {
        this.props.field.validate()
    }

    render() {
        const { hidden, field, label, id } = this.props
        const { value, mandatory, status, errors } = field

        return (
            <FormField
                id={id}
                label={label}
                mandatory={mandatory}
                hidden={hidden}
                errors={errors}
                status={status}
            >
                <Input
                    id={id}
                    name={id}
                    className={`text-input-${id}`}
                    type="text"
                    value={value}
                    onBlur={this.handleBlur}
                    onChange={this.handleChange}
                    placeholder={`Enter ${label.toLowerCase()}`}
                />
            </FormField>
        )
    }
}

export default observer(InputFormField)
