import React from 'react'

import { storiesOf } from '@storybook/react'
import { withKnobs } from '@storybook/addon-knobs'
// import { action } from '@storybook/addon-actions'
// import { linkTo } from '@storybook/addon-links'
import { Form } from 'reactstrap'
import BaseFieldSimulator from 'stores/ClaimStore/Field/BaseFieldSimulator'
import ButtonSelectFormField from './index'

storiesOf('ButtonSelectFormField', module)
    .addDecorator(withKnobs)
    .addDecorator(story => (
        <div style={{ padding: '20px', backgroundColor: '#f8f8f8' }}>
            <Form>{story()}</Form>
        </div>
    ))
    .add('simple', () => (
        <BaseFieldSimulator>
            {field => (
                <ButtonSelectFormField
                    id="simpleId"
                    label="simple label"
                    field={field}
                    options={[
                        { id: '1', label: 'Option One' },
                        { id: '2', label: 'Option Two' },
                        { id: '3', label: 'Option Three' },
                        { id: '4', label: 'Option Four' },
                        { id: '5', label: 'Option Five' },
                        { id: '6', label: 'Option Six' }
                    ]}
                    optionLabel={({ label }) => label}
                    optionDisabled={({ id }) => id === '2' || id === '5'}
                />
            )}
        </BaseFieldSimulator>
    ))
