import React, { Component } from 'react'
import { Button } from 'reactstrap'
import { observer } from 'mobx-react'
import FormField from 'components/FormField'
import styled from 'styled-components'
import theme from 'styles/theme'

const StyledButton = styled(Button)`
    width: 146px;
    height: 58px;
    white-space: normal;
    line-height: 1.1;
    letter-spacing: 0.05rem;
    box-shadow: 0 1px 1px 1px rgba(0, 0, 0, 0.08);

    &.btn-primary:not([disabled]):not(.disabled).active {
        /* border-color: #fff; */
    }

    &.btn-outline-primary {
        color: ${theme.jaguarBlack};

        &:hover {
            color: #fff;
        }
    }

    &.btn-primary.disabled {
        color: ${theme.jaguarFlatGrey2};
        background-color: ${theme.jaguarFlatGrey4};
        border-color: transparent;
    }
`

const StyledFormField = styled(FormField)`
    .validations {
        margin-top: 19px;
    }
`

class ButtonSelectFormField extends Component {
    handleChange = entry => () => {
        if (this.isCurrentSelectedValue(entry)) {
            // Unselect
            this.props.field.set()
        } else {
            // Select property
            this.props.field.set(entry)
        }
        this.props.field.validate()
    }

    isCurrentSelectedValue = option =>
        // Why comparing by reference instead of id is not working ?
        !!(this.props.field.value && this.props.field.value.id === option.id)

    render() {
        const {
            label,
            id,
            field,
            options,
            optionLabel,
            optionDisabled
        } = this.props
        const { mandatory, status, errors } = field

        // Convert options provided into options for select
        const selectOptions = options.map(entry => ({
            value: entry.id,
            label: optionLabel ? optionLabel(entry) : entry.id,
            disabled: optionDisabled && optionDisabled(entry),
            entry
        }))

        return (
            <StyledFormField
                id={id}
                label={label}
                mandatory={mandatory}
                errors={errors}
                status={status}
            >
                {selectOptions.map(option => (
                    <StyledButton
                        color="primary"
                        outline={
                            !option.disabled &&
                            !this.isCurrentSelectedValue(option.entry)
                        }
                        className="text-uppercase m-1 py-0"
                        key={option.value}
                        disabled={option.disabled}
                        active={this.isCurrentSelectedValue(option.entry)}
                        onClick={this.handleChange(option.entry)}
                    >
                        {option.label}
                    </StyledButton>
                ))}
            </StyledFormField>
        )
    }
}

export default observer(ButtonSelectFormField)
