import React from 'react'
import { CardBody, CardFooter } from 'reactstrap'
import styled from 'styled-components'
import theme from 'styles/theme'

import Tooltip from './Tooltip'

const StyledCardFooter = styled(CardFooter)`
    height: 46px;
    background-color: ${theme.jaguarBlueGrey3};
    font-size: 15px;
    font-weight: bold;
    text-transform: uppercase;
`

const StyledCardBody = styled(CardBody)`
    font-size: 15px;
`

const Content = ({ subtitle, children, tooltip }) => (
    <div>
        {subtitle && (
            <StyledCardFooter>
                {subtitle} <Tooltip>{tooltip}</Tooltip>
            </StyledCardFooter>
        )}
        <StyledCardBody>{children}</StyledCardBody>
    </div>
)

export default Content
