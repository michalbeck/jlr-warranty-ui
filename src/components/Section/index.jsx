import React, { Component } from 'react'
import { Card, CardHeader, Badge, Col, Row, Collapse } from 'reactstrap'
import styled from 'styled-components'
import theme from 'styles/theme'
import closeItem from 'assets/close-item.svg'
import openItem from 'assets/open-item.svg'
import Content from './Content'
import Tooltip from './Tooltip'

const Title = styled.span`
    text-transform: uppercase;
    padding-left: 15px;
    font-size: 15px;
    color: ${theme.jaguarBlack};
    font-weight: bold;
`

const Link = styled.a`
    float: right;
    padding-left: 5px;
    & .fa {
        color: ${theme.jaguarBlack};
    }
`

const StyledBadge = styled(Badge)`
    width: 24px;
    height: 24px;
    border: 2px solid #fff;
    border-radius: 12px;
    background-color: ${theme.jaguarRed};
`

const StyledCardHeader = styled(CardHeader)`
    height: 46px;
    background-color: ${theme.jaguarBlueGrey2};
`

class Section extends Component {
    static Content = Content

    state = {
        collapsed: false
    }

    handleToggleCollapsed = event => {
        event.preventDefault()
        this.setState(({ collapsed }) => ({
            collapsed: !collapsed
        }))
    }

    render() {
        const { order, title, children, tooltip } = this.props
        const { collapsed } = this.state
        return (
            <Row className="mb-3">
                <Col>
                    <Card>
                        <StyledCardHeader>
                            <h5 className="m-0">
                                <StyledBadge>{order}</StyledBadge>
                                <Title>{title}</Title>
                                {children && (
                                    <Link
                                        href="#"
                                        onClick={this.handleToggleCollapsed}
                                    >
                                        {!collapsed && (
                                            <img
                                                src={closeItem}
                                                alt="Close"
                                                title="Close"
                                            />
                                        )}
                                        {collapsed && (
                                            <img
                                                src={openItem}
                                                alt="Open"
                                                title="Open"
                                            />
                                        )}
                                    </Link>
                                )}
                                <Tooltip>{tooltip}</Tooltip>
                            </h5>
                        </StyledCardHeader>
                        <Collapse isOpen={!collapsed}>{children}</Collapse>
                    </Card>
                </Col>
            </Row>
        )
    }
}

export default Section
