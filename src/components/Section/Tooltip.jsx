import React, { Component } from 'react'
import { Tooltip } from 'reactstrap'
import styled from 'styled-components'
import QuestionMark from 'assets/question-red.svg'

const Link = styled.a`
    float: right;
    & .fa {
        color: #000;
    }
`

const TooltipImage = styled.img`
    position: relative;
    bottom: 3px;
`

class TTip extends Component {
    state = {
        open: false
    }

    handleToggle = () => {
        this.setState(({ open }) => ({
            open: !open
        }))
    }

    render() {
        const { children } = this.props
        const { open } = this.state

        if (!children) return null

        return (
            <Link
                innerRef={link => {
                    this.link = link
                }}
                href="#"
                onClick={event => event.preventDefault()}
            >
                <TooltipImage src={QuestionMark} alt="Question Mark" />
                {this.link && (
                    <Tooltip
                        placement="bottom-end"
                        isOpen={open}
                        toggle={this.handleToggle}
                        target={this.link}
                    >
                        {children}
                    </Tooltip>
                )}
            </Link>
        )
    }
}

export default TTip
