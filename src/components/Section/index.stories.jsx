import React from 'react'

import { storiesOf } from '@storybook/react'
import { withKnobs, text } from '@storybook/addon-knobs'
// import { action } from '@storybook/addon-actions'
// import { linkTo } from '@storybook/addon-links'
import Section from './index'

storiesOf('Section', module)
    .addDecorator(withKnobs)
    .addDecorator(story => <div style={{ margin: '20px' }}>{story()}</div>)
    .add('empty', () => <Section order="10" title="An empty section" />)
    .add('with tooltip', () => (
        <Section
            order="10"
            title="An empty section"
            tooltip="this is my help text displayed a tooltip"
        />
    ))
    .add('with some content', () => (
        <Section
            order={text('Order Text', 11)}
            title={text('Title', 'With some content section')}
            tooltip="this is my help text displayed a tooltip"
        >
            <Section.Content>Some content</Section.Content>
        </Section>
    ))
    .add('with content and subtitle with tooltip', () => (
        <Section
            order={text('Order Text', 12)}
            title={text('Title', 'With some content section')}
        >
            <Section.Content>Some content 1</Section.Content>
            <Section.Content
                subtitle="My subtitle"
                tooltip="this is my subsection content help text displayed a tooltip"
            >
                Some content 2
            </Section.Content>
        </Section>
    ))
