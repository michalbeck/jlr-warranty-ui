import { types, flow, getParent } from 'mobx-state-tree'

export const Authorisation = types.model('Authorisation', {
    id: types.identifier(types.string),
    action: types.string,
    claimType: types.string,
    partner: types.string,
    country: types.string
})

export const SecurityStore = types
    .model({
        service: 'lookupauthsecurity/read',
        loading: types.boolean,
        items: types.array(Authorisation)
    })
    .views(self => ({
        get app() {
            return getParent(self)
        }
    }))
    .actions(self => {
        const fetchAuthorisations = flow(function* fetchAuthorisations() {
            self.loading = true
            try {
                const json = yield self.app.fetch(self.service)
                self.items = json.records
                self.loading = false
            } catch (error) {
                self.loading = false
                /* console.error('Failed to load countries: ', error) */
            }
        })

        return { fetchAuthorisations }
    })
