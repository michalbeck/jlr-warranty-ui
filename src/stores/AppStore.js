import { types, getEnv } from 'mobx-state-tree'
import CountryStore from './CountryStore'
import RetailerStore from './RetailerStore'
import ClaimStore from './ClaimStore'

const AppStore = types
    .model('AppStore', {
        countryStore: types.optional(CountryStore, {}),
        retailerStore: types.optional(RetailerStore, {}),
        claimStore: types.optional(ClaimStore, {})
    })
    .views(self => ({
        get fetch() {
            return getEnv(self).fetch
        },
        get loading() {
            return self.countryStore.loading && self.retailerStore.loading
        },
        get authorisations() {
            return self.securityStore.items
        },
        get countries() {
            return self.countryStore.items
        },
        get retailers() {
            return self.retailerStore.items
        },
        get claim() {
            return self.claimStore.fields
        }
    }))

export default AppStore
