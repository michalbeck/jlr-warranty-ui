import { types, flow } from 'mobx-state-tree'
import request from 'services'
import Country from './Country'

const CountryStore = types
    .model({
        loading: false,
        items: types.optional(types.array(Country), [])
    })
    .actions(self => {
        const fetchCountries = flow(function* fetchCountries() {
            self.loading = true
            try {
                const json = yield request('lookupcountrylist/read')
                self.items = json.records
                self.loading = false
            } catch (error) {
                self.loading = false
                // TODO Error Store instead of console.error?
                console.error('Failed to load countries: ', error)
            }
        })

        function afterCreate() {
            fetchCountries()
        }

        return { afterCreate }
    })

export default CountryStore
