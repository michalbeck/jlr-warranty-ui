import { types } from 'mobx-state-tree'

const Country = types.model('Country', {
    id: types.identifier(types.string),
    countryKey: types.string,
    countryName: types.string
})

export default Country
