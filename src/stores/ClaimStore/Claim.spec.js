import Vehicle from 'stores/Vehicle'
import request from 'services'
import { when } from 'mobx'
import Claim from './Claim'

jest.mock('services')

let claim = null

beforeEach(() => {
    claim = Claim.create({})
})

it('have default vehicle distance unit as KM with new status', async () => {
    expect(claim.vehicleDistanceUnit.value).toEqual('KM')
    expect(claim.vehicleDistanceUnit.status).toEqual('new')
})

it('should change distance unit when new vehicle is set', done => {
    request.mockReturnValueOnce(
        Promise.resolve({
            records: [{ distanceUnit: 'MILES' }]
        })
    )

    const vehicle1 = Vehicle.create({
        id: '123',
        vin: '123',
        registration: 'ABC',
        distanceUnit: 'MILES'
    })
    claim.vehicle.set(vehicle1)

    when(
        () => claim.vehicleDistanceUnit.status === 'validated',
        () => {
            expect(claim.vehicleDistanceUnit.value).toEqual('MILES')
            done()
        }
    )
})
