import { types, onSnapshot } from 'mobx-state-tree'
import { reaction } from 'mobx'
import Retailer from 'stores/RetailerStore/Retailer'
import Country from 'stores/CountryStore/Country'
import Vehicle from 'stores/Vehicle'
import moment from 'moment'
import Field from './Field'
import FieldOption from './FieldOption'
import { DistanceUnitType } from './Field/DistanceUnit'
import {
    regexp,
    server,
    mandatory,
    maxValue,
    minDate
} from './Field/validators'

const Claim = types
    .model({
        id: types.optional(types.identifier(types.String), () =>
            new Date().getTime().toString()
        ), // New claim does may not have an id, generate one if not
        country: types.optional(
            Field({ value: types.maybe(types.reference(Country)) }),
            {}
        ),
        retailer: types.optional(
            Field({ value: types.maybe(types.reference(Retailer)) }),
            {}
        ),
        retailerReference: types.optional(
            Field({
                validations: [
                    mandatory(),
                    regexp({
                        exp: /^[0-9]{5,6}[A-Z]$/,
                        message:
                            'Invalid format - reference must be 6 numbers followed by an uppercase letter, eg 123456X',
                        block: true
                    }),
                    server({
                        endpoint:
                            'parvalidate/{reference}/{requestType}retailerref',
                        params: [
                            {
                                name: 'dealership',
                                path: { node: '/retailer', property: 'id' }
                            },
                            { name: 'reference' },
                            { name: 'requestType', value: 'claim' }
                        ]
                    })
                ],
                resetOnChange: [{ node: '/retailer', property: 'id' }]
            }),
            {}
        ),
        jobCardNumber: types.optional(
            Field({
                resetOnChange: [{ node: '/retailer', property: 'id' }]
            }),
            {}
        ),
        vehicle: types.optional(
            Field({ value: types.maybe(types.reference(Vehicle)) }),
            {}
        ),
        vehicleDeliveredDate: types.optional(
            Field({
                value: types.maybe(types.Date),
                validations: [
                    mandatory({
                        message:
                            'Date entered is empty, invalid or out of range.'
                    })
                ]
            }),
            {}
        ),
        repairStartedDate: types.optional(
            Field({
                value: types.maybe(types.Date),
                validations: [
                    minDate('/vehicleDeliveredDate', 'delivered date')
                ]
            }),
            {}
        ),
        repairStoppedDate: types.optional(
            Field({
                value: types.maybe(types.Date),
                validations: [
                    minDate('/repairStartedDate', 'repair started date')
                ]
            }),
            {}
        ),
        vehicleCollectedDate: types.optional(
            Field({
                value: types.maybe(types.Date),
                validations: [
                    minDate('/repairStoppedDate', 'repair stopped date')
                ]
            }),
            {}
        ),
        vehicleDistance: types.optional(
            Field({
                validations: [
                    mandatory(),
                    regexp({
                        exp: /^[0-9]*$/,
                        message: 'Distance must have only numbers',
                        block: true
                    }),
                    maxValue({ value: 999999 })
                ]
            }),
            {}
        ),
        vehicleDistanceUnit: types.optional(
            Field({
                value: DistanceUnitType
            }),
            {}
        ),
        vehicleDistanceProgression: types.optional(
            Field({
                value: false,
                validations: []
            }),
            {}
        ),
        vehicleFullHistoryConfirmed: types.optional(
            Field({
                value: false,
                validations: []
            }),
            {}
        ),
        claimType: types.optional(
            Field({
                value: types.maybe(FieldOption)
            }),
            {}
        ),
        campaignCode: types.optional(
            Field({ value: types.maybe(FieldOption) }),
            {}
        ),
        campaignOption: types.optional(
            Field({ value: types.maybe(FieldOption) }),
            {}
        ),
        technicianID: types.optional(Field({}), {}),
        technicalRefType: types.optional(
            Field({ value: types.maybe(FieldOption), validations: [] }),
            {}
        ),
        technicalRefNumber: types.optional(Field({ validations: [] }), {})
    })
    .views(self => ({
        get daysInShop() {
            return self.vehicleDeliveredDate.value &&
                self.vehicleCollectedDate.value
                ? moment(self.vehicleCollectedDate.value).diff(
                      self.vehicleDeliveredDate.value,
                      'days'
                  )
                : undefined
        }
    }))
    .actions(self => ({
        afterCreate() {
            onSnapshot(self, snapshot => {
                if (process.env.NODE_ENV === 'development') {
                    console.log(snapshot)
                }
            })

            // If vehicle was changed and distance unit changed, set the new default unit
            self.reaction = reaction(
                () => self.vehicle.value && self.vehicle.value.distanceUnit,
                distanceUnit => {
                    if (distanceUnit) {
                        self.vehicleDistanceUnit.set(distanceUnit)
                        self.vehicleDistanceUnit.validate()
                    }
                }
            )
        },
        beforeDestroy() {
            self.reaction()
        }
    }))

export default Claim
