import { types } from 'mobx-state-tree'
import Claim from './Claim'

const ClaimStore = types
    .model({
        claim: types.maybe(types.reference(Claim))
    })
    .actions(self => ({
        newClaim() {
            self.claim = Claim.create({})
        }
    }))

export default ClaimStore
