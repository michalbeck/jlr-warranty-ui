import maxValue from './maxValue'

let validator = null

beforeEach(() => {
    validator = maxValue({ value: 999999 })
})

it('build a validation object with expression passed', async () => {
    expect((await validator.validate('100')).valid).toEqual(true)
    expect((await validator.validate('dfg')).valid).toEqual(false)
    expect((await validator.validate('100000000')).valid).toEqual(false)
})

it('provides messages to be displayed if failed', async () => {
    expect((await validator.validate('5000000000')).messages).toEqual([
        'Field has max. value of 999999'
    ])
    expect((await validator.validate('abc')).messages).toEqual([
        'Field has max. value of 999999'
    ])
})

it('does not provides messages to be displayed if succeded', async () => {
    expect((await validator.validate('300')).messages).toEqual(undefined)
})

it('should not block by default', () => {
    expect(validator.block).toEqual(false)
})
