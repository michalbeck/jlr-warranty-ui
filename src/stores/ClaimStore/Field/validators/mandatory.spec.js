import mandatory from './mandatory'

let validator = null

beforeEach(() => {
    validator = mandatory()
})

it('validates inputs correctly', async () => {
    expect((await validator.validate('')).valid).toEqual(false)
    expect((await validator.validate('dfg')).valid).toEqual(true)
})

it('provides default message to be displayed if failed', async () => {
    expect((await validator.validate('')).messages).toEqual([
        'The field is mandatory'
    ])
})

it('provides custom message to be displayed if failed', async () => {
    validator = mandatory({ message: 'custom validation message' })

    expect((await validator.validate('')).messages).toEqual([
        'custom validation message'
    ])
})

it('does not provides messages to be displayed if succeded', async () => {
    expect((await validator.validate('abc')).messages).toEqual(undefined)
})

it('should block', () => {
    expect(validator.block).toEqual(true)
})

it('have correct name', () => {
    expect(validator.name).toEqual('mandatory')
})
