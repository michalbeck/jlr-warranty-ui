// @flow
const maxValue = ({ value, block }: { value: number, block?: boolean }) => ({
    validate: async (input: string) => {
        const intValue = parseInt(input, 10)
        if (Number.isNaN(intValue) || intValue > value) {
            return {
                valid: false,
                messages: [`Field has max. value of ${value}`]
            }
        }
        return { valid: true }
    },
    block: !!block
})

export default maxValue
