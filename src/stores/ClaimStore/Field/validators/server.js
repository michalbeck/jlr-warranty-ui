// @flow
import request from 'services'

type Params = {
    name: string,
    value?: string,
    path?: {
        node: string, // /retailer (to access /retailer/value)
        property?: string // id for example (to access /retailer/value/id)
    }
}

export const urlBuilder = (
    {
        endpoint,
        params = []
    }: {
        endpoint: string,
        params: Array<Params>,
        block?: boolean
    },
    input: string,
    resolver: Function
): string =>
    params
        .reduce((url, next) => {
            let value = input

            if (next.value) {
                ;({ value } = next)
            } else if (next.path) {
                value = resolver(next.path)
            }

            return `${url.replace(`{${next.name}}`, value) +
                encodeURIComponent(next.name)}=${encodeURIComponent(value)}&`
        }, `${endpoint}?`)
        .slice(0, -1)

const server = ({
    endpoint,
    params = [],
    block
}: {
    endpoint: string,
    params: Array<Params>,
    block?: boolean
}) => ({
    validate: async (input: string, resolver: Function) => {
        // 1. Build URL using params
        const url = urlBuilder({ endpoint, params }, input, resolver)
        // 2. Execute service and return results
        const { records } = await request(url)
        const {
            valid,
            messageDesc
        }: {
            valid: false,
            messageDesc: 'Invalid response for validation request'
        } = records[0]

        return { valid, messages: [messageDesc] }
    },
    block: !!block,
    tracked: (resolver: Function): ?Function => {
        // Resolving nodes from mobx tree that are relevant
        const nodes = params
            .map(
                param =>
                    param.path ? resolver({ node: param.path.node }) : null
            )
            .filter(node => !!node)

        // If there are nodes we need to track, then return data function for reaction
        if (nodes.length) {
            return () => nodes.map(node => node && node.value)
        }

        return null
    }
})

export default server
