// @flow
const mandatory = ({ message }: { message?: string } = {}) => ({
    validate: async (input: string) =>
        input
            ? { valid: true }
            : {
                  valid: false,
                  messages: [message || 'The field is mandatory']
              },
    block: true,
    name: 'mandatory'
})

export default mandatory
