// @flow
import moment from 'moment'

const minDateValidator = (node: string, fieldName: string) => ({
    validate: async (input: number, resolver: Function) => {
        const currentDate = moment(input)
        const resolvedNode = resolver({ node })
        const minDate = resolvedNode.value && moment(resolvedNode.value)

        return minDate === null || currentDate.isBefore(minDate)
            ? {
                  valid: false,
                  messages: [`Selected date is before ${fieldName}`]
              }
            : { valid: true }
    },
    block: true,
    tracked: (resolver: Function): ?Function => {
        const dateNode = resolver({ node })
        return () => [dateNode.value]
    }
})

export default minDateValidator
