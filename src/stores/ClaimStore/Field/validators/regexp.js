// @flow
const regexp = ({
    exp,
    message,
    block
}: {
    exp: RegExp,
    message: string,
    block?: boolean
}) => ({
    validate: async (input: string) =>
        exp.test(input)
            ? { valid: true }
            : { valid: false, messages: [message] },
    block: !!block
})

export default regexp
