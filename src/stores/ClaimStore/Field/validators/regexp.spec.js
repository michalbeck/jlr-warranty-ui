import regexp from './regexp'

let validator = null

beforeEach(() => {
    validator = regexp({ exp: /abc/, message: 'Unable to validate code' })
})

it('build a validation object with expression passed', async () => {
    expect((await validator.validate('abc')).valid).toEqual(true)
    expect((await validator.validate('dfg')).valid).toEqual(false)
})

it('provides messages to be displayed if failed', async () => {
    expect((await validator.validate('dfg')).messages).toEqual([
        'Unable to validate code'
    ])
})

it('does not provides messages to be displayed if succeded', async () => {
    expect((await validator.validate('abc')).messages).toEqual(undefined)
})

it('should not block by default', () => {
    expect(validator.block).toEqual(false)
})
