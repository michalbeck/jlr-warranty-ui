import moment from 'moment'
import minDate from './minDate'

let validator = null

beforeEach(() => {
    validator = minDate('/xyz', 'min date field name')
})

it('validates date inside range', async () => {
    // 15.01.2018 is after 13.01.2018
    expect(
        (await validator.validate(
            moment('15-01-2018', 'DD-MM-YYYY').valueOf(),
            () => ({ value: moment('13-01-2018', 'DD-MM-YYYY').valueOf() })
        )).valid
    ).toEqual(true)
})

it('validates date outside range', async () => {
    // 15.01.2018 is not after 18.01.2018
    const { valid, messages } = await validator.validate(
        moment('15-01-2018', 'DD-MM-YYYY').valueOf(),
        () => ({ value: moment('18-01-2018', 'DD-MM-YYYY').valueOf() })
    )

    expect(valid).toEqual(false)
    expect(messages[0]).toEqual('Selected date is before min date field name')
})

it('validates date against null minDate', async () => {
    // 15.01.2018 is not after 18.01.2018
    const { valid, messages } = await validator.validate(
        moment('15-01-2018', 'DD-MM-YYYY').valueOf(),
        () => ({ value: null })
    )

    expect(valid).toEqual(false)
    expect(messages[0]).toEqual('Selected date is before min date field name')
})

describe('tracked fields', () => {
    it('returns tracked fields correctly', () => {
        const resolver = jest.fn()
        resolver.mockReturnValueOnce({ value: '111' })

        const dataFunc = validator.tracked(resolver)
        expect(dataFunc).toBeInstanceOf(Function)
        expect(dataFunc()).toEqual(['111'])
    })

    it('calls resolver with right params', () => {
        const resolver = jest.fn()
        resolver.mockReturnValueOnce({ value: '111' })

        validator.tracked(resolver)

        expect(resolver).toHaveBeenCalledWith({ node: '/xyz' })
    })
})
