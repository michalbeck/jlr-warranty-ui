import request from 'services'
import server, { urlBuilder } from './server'

jest.mock('services')

describe('server', () => {
    let validator = null

    beforeEach(() => {
        validator = server({
            endpoint: 'parvalidate/{reference}/{requestType}retailerref',
            params: [
                {
                    name: 'dealership',
                    path: { node: '/retailer', property: 'id' }
                },
                { name: 'reference' },
                { name: 'requestType', value: 'claim' }
            ]
        })
    })

    it('calls the request with the right url', async () => {
        request.mockReturnValue({
            id: null,
            success: true,
            records: [{ id: null, messageDesc: '', valid: true }],
            errorMessages: []
        })
        await validator.validate('abc', () => 'id123')
        expect(request).toHaveBeenCalledWith(
            'parvalidate/abc/claimretailerref?dealership=id123&reference=abc&requestType=claim'
        )
    })

    it('returns valid response with correct format', async () => {
        request.mockReturnValue({
            id: null,
            success: true,
            records: [{ id: null, messageDesc: '', valid: true }],
            errorMessages: []
        })
        expect(await validator.validate('abc', () => {})).toEqual({
            valid: true,
            messages: ['']
        })
    })

    it('returns an error response with correct format', async () => {
        request.mockReturnValue({
            id: null,
            success: true,
            records: [
                {
                    id: null,
                    messageDesc: 'Claim was submitted too late',
                    valid: false
                }
            ],
            errorMessages: []
        })
        expect(await validator.validate('abc', () => {})).toEqual({
            valid: false,
            messages: ['Claim was submitted too late']
        })
    })

    describe('tracked fields', () => {
        beforeEach(() => {
            validator = server({
                endpoint: 'parvalidate/{reference}/{requestType}retailerref',
                params: [
                    {
                        name: 'dealership',
                        path: { node: '/retailer', property: 'id' }
                    },
                    { name: 'reference' },
                    { name: 'requestType', path: { node: '/claimType' } }
                ]
            })
        })

        it('returns tracked fields correctly', () => {
            const resolver = jest.fn()
            resolver
                .mockReturnValueOnce({ value: 'retailerId123' })
                .mockReturnValueOnce({ value: 'claimType123' })

            const dataFunc = validator.tracked(resolver)
            expect(dataFunc).toBeInstanceOf(Function)
            expect(dataFunc()).toEqual(['retailerId123', 'claimType123'])
        })

        it('calls resolver with right params', () => {
            const resolver = jest.fn()
            resolver
                .mockReturnValueOnce({ value: 'retailerId123' })
                .mockReturnValueOnce({ value: 'claimType123' })

            validator.tracked(resolver)

            expect(resolver).toHaveBeenCalledWith({ node: '/retailer' })
            expect(resolver).toHaveBeenCalledWith({ node: '/claimType' })
        })
    })
})

describe('urlBuilder', () => {
    it('builds a url without params', () => {
        const url = urlBuilder({
            endpoint: 'parvalidate/123/claimretailerref'
        })

        expect(url).toEqual('parvalidate/123/claimretailerref')
    })

    it('builds a url with own value if value is ommited', () => {
        const url = urlBuilder(
            {
                endpoint: 'parvalidate/{reference}/claimretailerref',
                params: [{ name: 'reference' }]
            },
            'AAA'
        )

        expect(url).toEqual('parvalidate/AAA/claimretailerref?reference=AAA')
    })

    it('builds a url with harcoded value if value is fixed', () => {
        const url = urlBuilder({
            endpoint: 'parvalidate/1234/{requestType}retailerref',
            params: [{ name: 'requestType', value: 'claim' }]
        })

        expect(url).toEqual(
            'parvalidate/1234/claimretailerref?requestType=claim'
        )
    })

    it('builds a url with path reference value if path is defined', () => {
        const url = urlBuilder(
            {
                endpoint: 'parvalidate/{reference}/claimretailerref',
                params: [{ name: 'reference', path: ['/retailer/value', 'id'] }]
            },
            'AAA',
            () => 'abcdef'
        )

        expect(url).toEqual(
            'parvalidate/abcdef/claimretailerref?reference=abcdef'
        )
    })
})
