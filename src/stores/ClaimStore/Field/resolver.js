import { resolvePath, getParent } from 'mobx-state-tree'

const resolver = self => ({ node, property }) => {
    // Resolve path from claim parent and find property
    // Need to have .value reference instead of resolvePath to /value
    // otherwise reactions won't work
    const leaf = resolvePath(getParent(self), node)
    return property && leaf && leaf.value ? leaf.value[property] : leaf
}

export default resolver
