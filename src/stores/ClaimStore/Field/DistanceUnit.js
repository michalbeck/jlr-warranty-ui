import { types } from 'mobx-state-tree'

const values = [
    {
        value: 'KM',
        label: 'Km'
    },
    {
        value: 'MILES',
        label: 'Miles'
    }
]

export const DistanceUnitType = types.optional(
    types.enumeration('DistanceUnit', values.map(unit => unit.value)),
    values[0].value
)

export default values
