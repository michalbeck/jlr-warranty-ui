import BaseField from './BaseField'
import { mandatory } from './validators'

let field = null

beforeEach(() => {
    field = BaseField({}).create({})
})

it('inits with empty errors', () => {
    expect(field.errors.length).toBe(0)
})

it('inits with empty string value', () => {
    expect(field.value).toBe('')
})

it('inits with mandatory by default', () => {
    expect(field.mandatory).toBeTruthy()
})

it('support custom type', () => {
    const customField = BaseField({ value: 3 }).create({
        id: 'customField'
    })
    expect(customField.value).toBe(3)
})

it('inits with new status', () => {
    expect(field.status).toBe('new')
})

it('retains new status when setting new value', () => {
    expect(field.status).toBe('new')
    expect(field.errors.toJSON()).toEqual([])
    field.set('abcde')
    expect(field.status).toBe('new')
    expect(field.errors.toJSON()).toEqual([])
})

it('reset to new status without errors when setting new value after unsuccessful validation', async () => {
    await field.validate()
    field.set('fghr')
    expect(field.status).toBe('new')
    expect(field.errors.toJSON()).toEqual([])
})

it('reset the validation errors when rerunning validation', async () => {
    await field.validate()
    await field.validate()
    expect(field.status).toBe('error')
    expect(field.errors.toJSON()).toEqual(['The field is mandatory'])
})

it('maintain the validation status when setting same value', async () => {
    field.set('abcd')
    await field.validate()
    expect(field.status).toBe('validated')
    field.set('abcd')
    expect(field.status).toBe('validated')
})

it('validates successfully mandatory on valid value', async () => {
    field.set('abcde')
    await field.validate()
    expect(field.status).toBe('validated')
    expect(field.errors.toJSON()).toEqual([])
})

it('validates with error mandatory on empty value', async () => {
    field.set('')
    await field.validate()
    expect(field.status).toBe('error')
    expect(field.errors.toJSON()).toEqual(['The field is mandatory'])
})

describe('defined validations', () => {
    const validator = (comp, block) => ({
        validate: async input =>
            Promise.resolve(
                input === comp
                    ? { valid: true }
                    : {
                          valid: false,
                          messages: [`my error message for ${comp}`]
                      }
            ),
        block
    })

    it('sets status to error and add error message on simple validation', async () => {
        field = BaseField({
            value: '',
            validations: [validator('a')]
        }).create({ mandatory: false })

        field.set('b')
        await field.validate()
        expect(field.status).toBe('error')
        expect(field.errors.toJSON()).toEqual(['my error message for a'])
    })

    it('sets status to validated on simple validation', async () => {
        field = BaseField({
            value: '',
            validations: [validator('a')]
        }).create({ mandatory: false })

        field.set('a')
        await field.validate()
        expect(field.status).toBe('validated')
        expect(field.errors.toJSON()).toEqual([])
    })

    it('multiple validate if value not valid', async () => {
        field = BaseField({
            value: '',
            validations: [validator('a'), validator('b')]
        }).create({ mandatory: false })

        field.set('c')
        await field.validate()
        expect(field.status).toBe('error')
        expect(field.errors.toJSON()).toEqual([
            'my error message for a',
            'my error message for b'
        ])
    })

    it('blocks further validations if value not valid', async () => {
        field = BaseField({
            value: '',
            validations: [validator('a', true), validator('b')]
        }).create({ mandatory: false })

        field.set('c')
        await field.validate()
        expect(field.status).toBe('error')
        expect(field.errors.toJSON()).toEqual(['my error message for a'])
    })

    it('blocks further validations for mandatory', async () => {
        field = BaseField({
            value: '',
            validations: [mandatory(), validator('a')]
        }).create({})

        field.set('')
        await field.validate()
        expect(field.status).toBe('error')
        expect(field.errors.toJSON()).toEqual(['The field is mandatory'])
    })
})
