import { Component } from 'react'

export default class BaseFieldSimulator extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: props.value,
            mandatory: props.mandatory || true,
            status: props.status || 'new',
            errors: props.errors || []
        }
    }

    handleSet = newValue => {
        this.setState(({ value }) => {
            if (value !== newValue) {
                return { value: newValue, status: 'new', errors: [] }
            }

            return { value: newValue }
        })
    }

    handleValidate = () => {
        this.setState(({ value }) => {
            if (value) {
                return { status: 'validated' }
            }

            return {
                status: 'error',
                errors: ['The field is mandatory']
            }
        })
    }

    render() {
        const { children } = this.props
        const set = this.handleSet
        const validate = this.handleValidate

        return children({ ...this.state, set, validate })
    }
}
