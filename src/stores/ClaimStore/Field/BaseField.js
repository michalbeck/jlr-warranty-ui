import { types, flow } from 'mobx-state-tree'
import { reaction } from 'mobx'
import { mandatory } from './validators'
import resolver from './resolver'

const Field = ({
    value = '',
    validations = [mandatory()],
    resetOnChange = []
}) =>
    types
        .model('Field', {
            status: types.optional(
                types.enumeration('Status', [
                    'new',
                    'pending',
                    'validated',
                    'error'
                ]),
                'new'
            ),
            errors: types.optional(types.array(types.string), [])
        })
        .views(() => ({
            get mandatory() {
                // Is mandatory if one of the validators is mandatory
                return !!validations.find(
                    validator => validator.name === 'mandatory'
                )
            }
        }))
        .actions(self => {
            let disposers = []

            const selfResolver = resolver(self)

            return {
                afterCreate() {
                    // Create reactions for validations if relevant
                    disposers = validations
                        .map(
                            validation =>
                                validation.tracked &&
                                validation.tracked(selfResolver)
                        )
                        .filter(tracked => !!tracked)
                        .map(tracked =>
                            reaction(tracked, ([data]) => {
                                // If tracked value has changed and this field was validated, validate again
                                if (data && self.status !== 'new') {
                                    self.validate()
                                }

                                // If data was removed, reset validations for this field
                                if (!data) {
                                    self.resetValidations()
                                    // Need to clear the field ?
                                }
                            })
                        )

                    // Create reaction for resetOnChange
                    if (resetOnChange.length) {
                        disposers.push(
                            reaction(
                                () =>
                                    resetOnChange.map(path =>
                                        selfResolver(path)
                                    ),
                                () => {
                                    self.resetValue()
                                    self.resetValidations()
                                }
                            )
                        )
                    }
                },
                beforeDestroy() {
                    // Destroy reactions
                    disposers.forEach(disposer => disposer())
                },
                resetValidations() {
                    self.status = 'new'
                    self.errors.clear()
                },
                resetValue() {
                    self.value = value
                },
                set(newValue) {
                    if (newValue !== self.value) {
                        // If value has changed, reset status and errors
                        self.resetValidations()
                    }
                    self.value = newValue
                },
                validate: flow(function* validate() {
                    self.status = 'pending'
                    self.errors.clear()

                    /* eslint-disable no-restricted-syntax */
                    // Executing defined validations until end or blocked
                    // Pushing messages to errors if they do not validate
                    for (const rule of validations) {
                        const { valid, messages = [] } = yield rule.validate(
                            self.value,
                            selfResolver
                        )

                        if (!valid) {
                            messages.forEach(msg => self.errors.push(msg))
                            self.status = 'error'
                            if (rule.block) {
                                break
                            }
                        }
                    }

                    // If after all error checking status is still pending, no errors were found
                    if (self.status === 'pending') {
                        self.status = 'validated'
                    }
                })
            }
        })
        .props({
            value
        })

export default Field
