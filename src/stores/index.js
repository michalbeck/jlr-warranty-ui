// import makeInspectable from 'mobx-devtools-mst'
import AppStore from './AppStore'

const createRootStore = snapshot => {
    const store = AppStore.create(snapshot, {})

    // makeInspectable(store)

    return store
}

export default createRootStore
