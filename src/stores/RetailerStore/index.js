import { types, flow, getRoot } from 'mobx-state-tree'
import request from 'services'
import { uniq, sortBy } from 'lodash'
import Retailer from './Retailer'

const RetailerStore = types
    .model({
        loading: false,
        items: types.optional(types.array(Retailer), [])
    })
    .views(self => ({
        countryById(countryId) {
            return getRoot(self).countryStore.items.find(
                country => country.id === countryId
            )
        },
        get countryOptions() {
            const retailerCountries = uniq(
                self.items.map(retailer => retailer.countryCode)
            )
            return sortBy(
                retailerCountries
                    .map(countryId => self.countryById(countryId))
                    .filter(country => country),
                'countryName'
            )
        },
        options(byCountry) {
            return self.items.filter(
                retailer => !byCountry || retailer.countryCode === byCountry.id
            )
        }
    }))
    .actions(self => {
        // TODO Make that a mixin for compose to avoid rewriting (also used in CountryStore)
        const fetchRetailers = flow(function* fetchRetailers() {
            self.loading = true
            try {
                const json = yield request('lookupdealers/read?withSor=true')
                self.items = json.records
                self.loading = false
            } catch (error) {
                self.loading = false
                // TODO Error Store instead of console.error?
                console.error('Failed to load retailers: ', error)
            }
        })

        function afterCreate() {
            fetchRetailers()
        }

        return { afterCreate }
    })

export default RetailerStore
