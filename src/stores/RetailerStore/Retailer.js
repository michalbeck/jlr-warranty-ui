import { types } from 'mobx-state-tree'

const Retailer = types.model('Retailer', {
    id: types.identifier(types.string),
    assignedPartner: types.string,
    brand: types.string,
    countryCode: types.string,
    countryDesc: types.string,
    currency: types.string,
    customerGroup: types.string,
    customerGroup1: types.string,
    dealerCode: types.string,
    dealerContact: types.string,
    description: types.string,
    direct: types.optional(types.boolean, false),
    distributor: types.string,
    name: types.string
    /* sorList: types.optional(types.array(), []), */
})

export default Retailer
