import { types, flow } from 'mobx-state-tree'
import format from 'string-format-obj'
import request from 'services'

const Vehicle = types
    .model('Vehicle', {
        id: types.identifier(types.string),
        vin: types.string,
        registration: types.string,
        description: types.maybe(types.string),
        distanceLastClaim: types.maybe(types.number),
        distanceUnit: types.maybe(types.string),
        dateLastClaim: types.maybe(types.Date),
        isLoading: false
    })
    .actions(self => ({
        afterCreate: () => {
            self.loadDetails()
        },
        loadDetails: flow(function* loadDetails() {
            try {
                self.isLoading = true
                const { records } = yield request(
                    format(
                        'vehicle/{id}?id={id}&dealershipContext={dealership}&priceDisplay=FULL',
                        {
                            id: self.id,
                            dealership: '0040010423'
                        }
                    )
                )
                // Update details
                self.description = records[0].description
                self.distanceLastClaim = records[0].distanceLastClaim
                self.distanceUnit = records[0].distanceUnit
                self.dateLastClaim = new Date(records[0].dispatchDate)
            } catch (error) {
                // TODO
                console.log(error)
            }

            self.isLoading = false
        })
    }))

export default Vehicle
