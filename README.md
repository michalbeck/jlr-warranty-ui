# jlr-warranty-ui
Jaguar Land Rover - warranty and claims data management tool

- React app using Mobx and Mobx State Tree for state management - `yarn start`
- Micro-dev server for mock API and validation - `yarn server`
- Storybook interactive component library for dev/testing - `yarn storybook`
- Loki visual regression testing (requires Docker) - `yarn test:visual`
- Nightwatch automated testing - `yarn bdd`, `yarn bdd:chrome`
