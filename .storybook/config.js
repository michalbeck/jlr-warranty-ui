import React from 'react'
import { configure, addDecorator } from '@storybook/react'
import 'loki/configure-react'
import { ThemeProvider } from 'styled-components'
import colours from 'styles/colours'
import 'styles/index.css'

const req = require.context('../src', true, /\.stories\.jsx$/)

function loadStories() {
    req.keys().forEach(filename => req(filename))
}

addDecorator(story => <ThemeProvider theme={colours}>{story()}</ThemeProvider>)

configure(loadStories, module)
