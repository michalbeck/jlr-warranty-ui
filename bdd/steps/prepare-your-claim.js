const { client } = require('nightwatch-cucumber')
const { defineSupportCode } = require('cucumber')

defineSupportCode(({ Given, Then, When }) => {
    let fieldSelector

    Given('I open Claim Create page', () =>
        client
            .url('http://localhost:3000/#/claim/create')
            .waitForElementVisible('main', 1000)
    )

    Given(
        'that I have access to Create Claims for Retailers in more than one country',
        () => {}
    )

    Given(
        'that I have access to Create Claims for Retailers in only one country',
        () => {}
    )

    Then(
        /^the "(.*?)" select field is( not)? displayed$/,
        (fieldName, notDisplayed) => {
            fieldSelector = `.select-input-${fieldName}`
            return notDisplayed
                ? client.assert.elementNotPresent(fieldSelector)
                : client.assert.elementPresent(fieldSelector)
        }
    )
    When(/^I select the "(.*?)" select field/, fieldName => {
        fieldSelector = `.select-input-${fieldName}`
        return client.click(fieldSelector)
    })
    Then('I should be presented with a drop-down menu', () =>
        client.assert.visible(`${fieldSelector} .Select-menu-outer`)
    )
    Then('I can select an option from that menu', () =>
        client
            .click(`${fieldSelector} .Select-menu-outer .Select-option`)
            .assert.value(`${fieldSelector} input`, 'CN')
    )
})
