Feature: JWP3-8 Prepare your claim
  Background:
    Given I open Claim Create page

  Scenario: JWP3-46: Country field is a drop-down menu
    Given that I have access to Create Claims for Retailers in more than one country
    When I select the "country" select field
    Then I should be presented with a drop-down menu
    And I can select an option from that menu

  Scenario: JWP3-44: Country field is displayed to the relevant retailers
    Given that I have access to Create Claims for Retailers in more than one country
    Then the "country" select field is displayed

  Scenario: JWP3-43: Country field is hidden to the relevant retailers
    Given that I have access to Create Claims for Retailers in only one country
    Then the "country" select field is not displayed